
# Custom shortcut commands
alias install="sudo apt-get install"
alias poweroff="sudo shutdown -h now"
alias logout="i3-msg exit"
alias cd..="cd .."
alias ll="ls -lhF"
alias la='ls -lhA'
alias llab='ssh andrea.muttoni@llab'
alias vas='ssh andrea.muttoni@vas.it'
alias ideas='ssh andrea.muttoni@193.205.23.138'
alias untar='tar -xvzf'
alias laptop='xrandr --output eDP1 --primary --auto --output DP1 --off'
alias dual='xrandr --output DP1 --mode 1680x1050 --above eDP1'
alias dualhdmi='xrandr --output HDMI1 --mode 1680x1050 --above eDP1'
alias naut='nautilus --no-desktop --new-window &'
alias brightness='xrandr --output eDP1 --brightness'
alias sublp='subl -n -a'
alias home='cd /home/$USER/'
# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    
    alias ls='ls --color=auto'
    
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias rgrep='rgrep --color=auto'
fi

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    
    alias ls='ls --color=auto'
    
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias rgrep='rgrep --color=auto'
fi

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

#Size of dir
function sizeof() { 
  if [ -n "$1" ]; then 
    du -hs $1;
  else 
    du -hs ".";
  fi
}

function sizetop() { 
  if [ -n "$1" ] && [ -n "$2" ]; then 
    du -a "$2" | sort -nr | head -n "$1";
  elif [ -n "$1" ]; then
    du -a "." | sort -nr | head -n "$1";
  else 
    du -a "." | sort -nr | head -n 10;
  fi
}
